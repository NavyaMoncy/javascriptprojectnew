document.getElementById('bta').addEventListener('click',sortedAscend);
document.getElementById('btd').addEventListener('click',sortedDescend);
document.getElementById('btadoj').addEventListener('click',sortedAscenddoj);
document.getElementById('btddoj').addEventListener('click',sortedDescenddoj);
document.getElementById('bt_new').addEventListener('click',displayNone);
document.getElementById('form').addEventListener('submit',getInput1);
document.getElementById('lastName').addEventListener('blur',validateLastName);
document.getElementById('phone').addEventListener('blur',validatePhone);
document.getElementById('email').addEventListener('blur',validateEmail);
document.getElementById("item-select").addEventListener('change',noItemPerPage);
document.getElementById('search-employee').addEventListener('keyup',searchEmployee);
var sal,count;
var dataAll;
var del=0;
var name;
var record;
var current_page = 1;
var records_per_page=2;
var counter=13;
records_per_page=document.getElementById('item-select').value;
console.log(records_per_page);
function noItemPerPage(e)
{
  records_per_page=e.target.value;
}
console.log(records_per_page);
function prevPage()
{
  if (current_page > 1) {
        current_page--;
        pageSelect();
        displayData(current_page);
     }
}
function nextPage()
{
    if (current_page < numPages()) {
      pageSelect();
        current_page++;
       displayData(current_page);
    }
}
function getJson(){
    fetch('database.json')
      .then(function(res){
          return res.json();
      })
     .then(function(data){
       
            dataAll=data; 
            console.log(dataAll);
            displayConsole(dataAll);
            //pageSelect();
            displayData(1);
            numPages(dataAll);
          })
    .catch(function(err){
        console.log(err);
    });
}

//------------------------DISPLAYING ALL DATA--------------------------//
function displayConsole(dataAll){
  console.log(dataAll);
}
console.log(dataAll);
function displayData(page)
    {
      console.log(page);
      pageSelect();
      var pagenext = document.getElementById("page-item-next");
      var pageprev = document.getElementById("page-item-prev");
      var page_span = document.getElementById("page");
      if (page < 1) page = 1;
      if (page > numPages()) page = numPages();
     
      console.log(dataAll);
      
      
        var table=document.getElementById('page2');
        table.innerHTML="";
        var j=0;
       
        for(var i=(page-1)*records_per_page;i<(page*records_per_page);i++,del=i)
        {
           
                     
                       del=i;
                      
            var row=`<tr>
                         <td>${dataAll[i].salutation}</td>
                         <td id="td_1">${dataAll[i].firstName}</td>
                         <td>${dataAll[i].lastName}</td>
                         <td>${dataAll[i].email}</td>
                         <td>${dataAll[i].gender}</td>
                         <td>${dataAll[i].phoneNo}</td>
                         <td>${dataAll[i].city}</td>
                         <td>${dataAll[i].state}</td>
                         <td>${dataAll[i].country}</td>
                         <td>${dataAll[i].doj}</td>
                         <td><button class="btn btn-outline-primary"id="delete-row" onclick="SomeDeleteRowFunction(${del})"><i class="fas fa-trash-alt"></i></button></td>
                         
                         <td>
                         <button id="btnUpdate" class="btn btn-outline-primary" value="Update"id="update-row" onclick="displayNoneUp(${dataAll[i].id})"><i class="fas fa-edit"></i></button
                         </td>
                     </tr>`
            table.innerHTML+=row;  
            
           pageSelect();
            
            console.log(del);

            }
            page_span.innerHTML = page;
            
          
         
         if (page == 1) {
          pageprev.style.visibility = "hidden";
      } else {
          pageprev.style.visibility = "visible";
      }
    
      if (page == numPages()) {
        pagenext.style.visibility = "hidden";
      } else {
          pagenext.style.visibility = "visible";
      }
      
    }

     function numPages()
     { pageSelect();
        console.log(dataAll);
       return Math.ceil(dataAll.length / records_per_page);
     }
     function pageSelect(){

      records_per_page=document.getElementById("item-select").value;
    
     
      
 
 }
    //--------------DISPLAYING DATA IN ASCENDING ORDER BASED ON NAME-------------//
     function sortedAscend(){
        console.log(dataAll);
        dataAll.sort((a, b) => {
           let fa = a.firstName.toLowerCase(),
           fb = b.firstName.toLowerCase();
           if (fa > fb) {
               return 1;
           }
           if (fa < fb) {
           return -1;
       }
       return 0;
   });
      document.getElementById('page2').innerHTML='';
     displayData(1);
       
 }
 //--------------DISPLAYING DATA IN DESCENDING ORDER BASED ON NAME--------//
function sortedDescend(){
       console.log(dataAll);
       dataAll.sort((a, b) => {
          let fa = a.firstName.toLowerCase(),
          fb = b.firstName.toLowerCase();
          if (fa < fb) {
              return 1;
          }
          if (fa > fb) {
          return -1;
      }
      return 0;
  });
     document.getElementById('page2').innerHTML='';
    displayData(1);
      
}

//----------------------DISPLAY DATA IN DESCENDIN ORDER BASED ON DATE OF JOIN-------------//
function sortedDescenddoj(){
    console.log(dataAll);
    dataAll.sort((a, b) => {
       let fa = Date.parse(a.doj);
       fb = Date.parse(b.doj);
       if (fa < fb) {
           return 1;
       }
       if (fa > fb) {
       return -1;
   }
   return 0;
});
  document.getElementById('page2').innerHTML='';
 displayData(1);
   
}

//--------------DISPLAY DATE IN ASCENDING ORDER BASED ON DATE OF JOIN-----------//
function sortedAscenddoj(){
    console.log(dataAll);
    dataAll.sort((a, b) => {
       let fa = Date.parse(a.doj);
       fb = Date.parse(b.doj);
       if (fa >fb) {
           return 1;
       }
       if (fa < fb) {
       return -1;
   }
   return 0;
});
  document.getElementById('page2').innerHTML='';
 displayData(1);
   
}
  function displayNone(){

  document.getElementById('add_form').style.display = "block"; 
 }
 function displayNoneUp(v){

  document.getElementById('add_form').style.display = "block";
  UpdateRowFunction(v); 

  console.log(del);
 }

//---------------DISPLAYING COUNTRY AND STATE IN SELECT----------------//
var stat;
function myCountry(){
    fetch('state.json')
    .then(function(res){
        return res.json();
    })
      
    .then(function(data){
          let output_sal='';
          console.log(data);
          var htr=document.getElementById("mySelect").value;
          console.log(htr);
          data.forEach(post => {
              if(post.id==htr){
                output_sal+=`
                <div class="input-group mb-3 ms-5 mt-3">
                <div class="input-group-prepend">
                      <label class="input-group-text" for="inputGroupSelect01">State</label>
                    </div>
                <select class="custom-select" id="myState"id="inputGroupSelect01" >
                  <option value="${post.state1}">${post.state1}</option>
                  <option value="${post.state2}">${post.state2}</option>
                  <option value="${post.state3}">${post.state3}</option>
                  <option value="${post.state4}">${post.state4}</option>
                  <option value="${post.state5}">${post.state5}</option>
                  <option value="${post.state6}">${post.state6}</option>
                  <option value="${post.state7}">${post.state7}</option>
                  </select></div>
                `;
                   
              }
          
      });
      document.querySelector('#myState').innerHTML=output_sal;  
      document.getElementById("myState").addEventListener('change',stateSelection);
                   
      
      
  })
  .catch(function(err){
          console.log(err);
      });
}
function stateSelection(e) {
  {
    stat=(e.target.value);

  }
  
}
//----------------------SEARCH EMPLOYEE--------------------//
function searchEmployee(e){
    console.log(e.target.value);
    var searchAll=dataAll;
    console.log(searchAll);
    let compfirst,complast;
    document.getElementById('page2').innerHTML=''; 
    var table=document.getElementById('page2');
    for(var i=0;i<searchAll.length;i++)
    {   
             compfirst=searchAll[i].firstName.toLowerCase();
             console.log(compfirst);
             complast=searchAll[i].lastName.toLowerCase();
            
             let len=e.target.value.toLowerCase();
            
            
          if((compfirst.includes(len))||(complast.includes(len)))
          {
            
            console.log('Matches');
            
            
            var row=`<tr>
            <td>${searchAll[i].salutation}</td>
            <td>${searchAll[i].firstName}</td> 
            <td>${searchAll[i].lastName}</td>
            <td>${searchAll[i].email}</td>
            <td>${searchAll[i].gender}</td>
            <td>${searchAll[i].phoneNo}</td>
            <td>${searchAll[i].city}</td>
            <td>${searchAll[i].state}</td>
            <td>${searchAll[i].country}</td>
            <td>${searchAll[i].doj}</td>
            
            <td><input type="button" class="btn btn-outline-danger" value="Delete" id="delete-row" onclick="SomeDeleteRowFunction(${del})"></td>
            
            <td>
            <button type="button" class="btn btn-outline-success">
            ${dataAll[i].update}</button>
            </td>
            </tr>`
            table.innerHTML+=row; 

          }
          else{
            console.log('NOT Matches');
          }
        }
     }
    function SomeDeleteRowFunction(v) {
    console.log(v);
    if(confirm('Are you sure to delete'))
    {
        dataAll.splice(v,1);
        console.log(dataAll);
        document.getElementById('page2').innerHTML='';
        displayData(1);
        alert('Successfully Deleted');
        
   }

}

//-----------------------GETTING DATA FROM THE USER------------------------//
var index=-1;
function getInput1(e){
  console.log('hai');
  e.preventDefault();
  var fname=document.getElementById('firstName').value;
  var lname=document.getElementById('lastName').value;
  var phone=document.getElementById('phone').value;
  var email=document.getElementById('email').value;
  var city=document.getElementById('city').value;
  console.log(index);
  
  let sex;
    if (document.getElementById('male').checked) {
        sex = document.getElementById('male').value;
        console.log(sex);
      }
      else if (document.getElementById('female').checked) {
        sex = document.getElementById('female').value;
        console.log(sex);
      }
      mySal();
    myCount();
    var dateTime=new Date();
    var date = new Date(dateTime.getTime());
    date.setHours(0, 0, 0, 0);
    var day=date.getDate();
    var month=date.getMonth();
    var year=date.getFullYear();
    var shortDate = month + "/" + day + "/" + year;
    var stringDate=shortDate.toString();
    
      if(validateLastName()==true){
        console.log('good you did it');
            
      }
      else{
            lname=' ';
            fname=' ';
            console.log('Sry baby you gone wrong');
          }
          if(validatePhone()==true){
         
            console.log('good you did it');
                  
           }
          else{
            phone=' ';
            console.log('Sry baby you gone wrong');
          } 
           if(validateEmail()==true){
         
             console.log('good you did it');
                        
          }
          else{
             email=' ';
            console.log('Sry baby you gone wrong');
          }  
        if((index==-1))
        {
          if(fname!==' ')
          {
              console.log(fname+' '+lname+' '+phone+' '+email+' '+city+' '+sex+' '+sal+' '+count);
              dataAll.push({"email":email,"firstName":fname,"lastName":lname,"salutation":sal,"gender":sex,"city":city,"phoneNo":phone,"doj":stringDate,"country":count,"state":stat,"id":counter});
          }
    
      }
      else{
        dataAll[index].firstName=fname;
        dataAll[index].lastName=lname;
        dataAll[index].city=city;
        dataAll[index].phoneNo=phone;
        dataAll[index].email=email;
        dataAll[index].country=count;
        dataAll[index].state=stat;
        dataAll[index].gender=sex;
        dataAll[index].salutation=sal;
        index=-1;
      }

      
      document.getElementById('page2').innerHTML='';
      closeForm();
      displayData(1);
}



function mySal(){

     sal=document.getElementById("salutation").value;
     
     //stat=document.getElementById("salutation").value;

}
function myCount(){
    count=document.getElementById("mySelect").value;
    console.log(count);
}

//---------------------FORM VALIDATION--------------//

function closeForm() {

    document.getElementById('firstName').value='';
    document.getElementById('lastName').value='';
    document.getElementById('email').value='';
    document.getElementById('phone').value='';
    document.getElementById('city').value='';
    
    document.getElementById('lastName').classList.remove('is-invalid');
    document.getElementById('firstName').classList.remove('is-invalid');
    document.getElementById('phone').classList.remove('is-invalid');
    document.getElementById('email').classList.remove('is-invalid');
    document.getElementById("add_form").style.display = "none";
  }
  
function validateLastName(e){
   let fname=document.getElementById('firstName').value;
    let lname=document.getElementById('lastName').value;
    console.log(fname);
    console.log(lname);
    let x=true;
    var fnameLower=fname.toLowerCase();
    var lnameLower=lname.toLowerCase();
    var fLower,lLower;

    for(i=0;i<dataAll.length;i++)
    {
       fLower=dataAll[i].firstName.toLowerCase();
    

        if(fnameLower==fLower)
        { console.log('invalid');
          for(j=0;j<dataAll.length;j++)
          {
            lLower=dataAll[i].lastName.toLowerCase();
            if(lnameLower==lLower)
            {
              document.getElementById('error').style.display = "block"; 
              console.log('invalid');
              x=false;
            }
            else{
              document.getElementById('error').style.display = "none";
              x=true;
            }
          }

        }
        return x;
    }
    //var name1=e.target.value;
    
}
function validatePhone() {
    const phone = document.getElementById('phone');
    const re = /^\(?\d{3}\)?[-. ]?\d{3}[-. ]?\d{4}$/;
    let x=true
    if(!re.test(phone.value)){
      phone.classList.add('is-invalid');
      x=false;
    } else {
      phone.classList.remove('is-invalid');
      x=true;
    }
    return x;
  }
  function validateEmail() {
    const email = document.getElementById('email');
    const re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
     let x;
    if(!re.test(email.value)){
      email.classList.add('is-invalid');
      x=false;
    } else {
      email.classList.remove('is-invalid');
      x=true;
    }
    return x;
  }
  window.onload = function() {
    
};

function UpdateRowFunction(v){
  console.log(v);
  
 // console.log(v);
 
 for(i=0;i<dataAll.length;i++){
    if(dataAll[i].id==v)
    {
      index=i;
    }
 }
 var hum=dataAll[index].gender;
 if(hum=='male'){
  document.getElementById("male").checked = true;

 }
 if(hum=='female'){
  document.getElementById("female").checked = true;

 }
 document.getElementById('firstName').value=dataAll[index].firstName;
 document.getElementById('lastName').value=dataAll[index].lastName;
 document.getElementById('email').value=dataAll[index].email;
 document.getElementById('city').value=dataAll[index].city;
 document.getElementById('phone').value=dataAll[index].phoneNo;
}
getJson();
myCountry();